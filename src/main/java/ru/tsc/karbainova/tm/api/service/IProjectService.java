package ru.tsc.karbainova.tm.api.service;

import ru.tsc.karbainova.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    void create(String name);

    void create(String name, String description);

    void add(Project project);

    void remove(Project project);

    List<Project> findAll();

    List<Project> findAll(Comparator<Project> comparator);

    void clear();

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    Project removeByName(String name);

    Project findById(String id);

    Project findByIndex(Integer index);

    Project findByName(String name);

    Project startById(String id);

    Project startByIndex(Integer index);

    Project startByName(String name);

    Project finishById(String id);

    Project finishByIndex(Integer index);

    Project finishByName(String name);
}
